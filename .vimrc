filetype plugin indent on
set tabstop=4
set shiftwidth=4
set nocompatible
set expandtab
set number
syntax on
